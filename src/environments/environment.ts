// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyA9ii2yJGZ3A90Pxr1IbLsKY0QQWcSVQ14",
    authDomain: "vacunacion-srn.firebaseapp.com",
    databaseURL: "https://vacunacion-srn-default-rtdb.firebaseio.com",
    projectId: "vacunacion-srn",
    storageBucket: "vacunacion-srn.appspot.com",
    messagingSenderId: "39005318930",
    appId: "1:39005318930:web:9028887d7975520ad04dcb"
  

  } 
};
