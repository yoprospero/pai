import { Injectable } from '@angular/core';
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';
import { classPersona } from '../demo/domain/classPersona';

@Injectable()
export class DatosService {

  PersonasList:AngularFireList<any>;
  
  selectePersona:classPersona=null;
  constructor(private fireData:AngularFireDatabase) { }
 
  getPersonas(){

    return this.PersonasList =this.fireData.list('personas')

  }

  insertPersona(persona:classPersona){

  
    this.PersonasList.set(persona.tipo_id['code']+persona.id,{
      tipo_id: persona.tipo_id,
      id: persona.id,
      apellido_1: persona.apellido_1,
      apellido_2: persona.apellido_2,
      nombre_1: persona.nombre_1,
      nombre_2: persona.nombre_2,
      fec_nac: persona.fec_nac,
      sexo: persona.sexo,
      lugar_naci:persona.lugar_naci,
      empresa_labora:persona.empresa_labora,
      regimen		:persona.regimen,
      localidad	: persona.localidad,
      barrio	:persona.barrio,
      direccion	:persona.direccion,
      fijo:persona.fijo,
      celular	:persona.celular,
      correo:persona.correo,
      tel_contact:persona.tel_contact,
      lote_v:persona.lote_v,
      laboratorio:persona.laboratorio
      

    });

  }

  remover(key:string){

this.PersonasList.remove(key);


  }
}