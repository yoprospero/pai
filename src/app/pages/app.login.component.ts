import { Component } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
// import firebase from 'firebase/';
import 'firebase/auth';
import firebase from '@firebase/app';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './app.login.component.html',
})
export class AppLoginComponent {

    //@Input() action:string;

    email: string;
    password: string;
    errorMessage = '';

    constructor(
      public auth: AngularFireAuth,
      private router: Router) { }

//prueba comentario
    ngOnInit() {
      //console.log(this.action);
    }

/*     loginWith() {
      this.auth.signInWithPopup(new auth.GoogleAuthProvider);
    } */
    logout() {
      this.auth.signOut();
    }
    showData(){
      this.auth.user.subscribe( res => {
        console.log(res);
      });
    }

    // register(){
    //   this.auth.createUserWithEmailAndPassword(this.email,this.pass)
    //   .then((user) => {
    //     // Signed in
    //     // ...
    //     console.log(user);
    //     this.router.navigate(['profile']);
    //   })
    //   .catch((error) => {
    //     const errorCode = error.code;
    //     const errorMessage = error.message;
    //     console.log(errorCode,': ',errorMessage);
    //   });
    // }

    customLogin(){
      this.auth.signInWithEmailAndPassword(this.email,this.password)
      .then( res => {
        this.router.navigate(['/']);
      })
      .catch(response => {
        this.errorMessage = response.message;
      });
    }

}
