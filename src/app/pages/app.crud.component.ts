import {Component, OnInit} from '@angular/core';
//import {Product} from '../demo/domain/product';
import {classPersona} from '../demo/domain/classPersona';
import {DatosService} from '../services/datos.service';
import {ProductService} from '../demo/service/productservice';
import {ConfirmationService, MessageService} from 'primeng/api'
import {NgForm} from '@angular/forms';import { ThirdPartyDraggable } from '@fullcalendar/interaction';
;

@Component({
    templateUrl: './app.crud.component.html',
    styleUrls: ['../demo/view/tabledemo.scss'],
    styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }

        @media screen and (max-width: 960px) {
            :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:last-child {
                text-align: center;
            }

            :host ::ng-deep .p-datatable.p-datatable-customers .p-datatable-tbody > tr > td:nth-child(6) {
                display: flex;
            }
        }

    `],
    providers: [MessageService, ConfirmationService]
})
export class AppCrudComponent implements OnInit {

    productDialog: boolean;

    tablaList: classPersona[];

    personaEdit:classPersona;

    selectedPersonas: classPersona[];

    submitted: boolean;



    cols = [
        {field: 'tipo_id', header: 'tipo_id'},
        {field: 'id', header: 'id'},
        {field: 'nombre_1', header: 'nombre_1'},
        {field: 'nombre_2', header: 'nombre_2'},
        {field: 'apellido_1', header: 'apellido_1'},
        {field: 'apellido_2', header: 'apellido_2'},
    ];

    tipos_ident: any[] = [
        {name: 'Cedula de Ciudadania', code: 'CC'},
        {name: 'Cedula de Extranjeria', value: 'CE'},
        {name: 'Pasaporte', code: 'PA'},
        {name: 'Permiso Especial de permanencia', code: 'PE'},
        {name: 'Tarjeta de Identidad', code: 'TI'}
    ];

    sexos: any[] = [
        {name: 'Masculino', code: 'Masculino'},
        {name: 'Femenino', value: 'Femenino'}

    ];
    constructor(private productService: ProductService, private messageService: MessageService,
                private confirmationService: ConfirmationService,private datosService:DatosService) {}

    ngOnInit() {
        //this.productService.getProducts().then(data => this.products = data);


            this.datosService.getPersonas().snapshotChanges().subscribe(item => {
                this.tablaList = [];
                item.forEach(element => {
                  let x = element.payload.toJSON();
                  x["key"] = element.key;
                  this.tablaList.push(x as classPersona);
                });
              });

    }

    openNew() {

        this.personaEdit={...new classPersona()}
        this.submitted = false;
        this.productDialog = true;
    }

    deleteSelectedPersonas() {
        this.confirmationService.confirm({
            message: 'Está seguro de eliminar los '+this.selectedPersonas.length +' registros seleccionados.',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.selectedPersonas.forEach(elemn =>{

                    this.datosService.remover(elemn.key);
                })
                this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Personas eliminadas.', life: 3000});
            }
        });
    }

    editProduct(persona: classPersona) {
       this.submitted=false;
        this.personaEdit={...persona};
        this.productDialog = true;
       // this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});

    }
    deletePersona(persona: classPersona) {
        this.confirmationService.confirm({
            message: 'Esta seguro/a de eliminar a ' + persona.nombre_1 +' '+ persona.apellido_1 +'?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.datosService.remover(persona.key);

                this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Persona eliminada.', life: 3000});
            }
        });
    }

    hideDialog() {
        this.productDialog = false;
       // this.submitted = false;
    }

    onSubmit(personaForm: NgForm)
    {

        this.submitted = true;
        if(personaForm.value.id && personaForm.value.tipo_id ){

                   this.datosService.insertPersona(personaForm.value)
                    this.resetForm(personaForm);
                    this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Registro guardado', life: 3000});
                    this.productDialog = false;
        }else{
            this.messageService.add({severity: 'error', summary: 'Error', detail: 'Faltan campos.', life: 3000});
        }
    }
    resetForm(personaForm?: NgForm)
    {
      if(personaForm != null){

        personaForm.reset();
        this.personaEdit={...new classPersona()}

      }

    }
    saveProduct() {
        //this.submitted = true;

      /*  if (this.persona.id.trim()) {
            if (this.persona.id) {
               this.datosService.insertPersona(this.persona);
                this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
            } else {
             //   this.product.id = this.createId();
              //  this.product.image = 'product-placeholder.svg';
               // this.products.push(this.product);
                this.messageService.add({severity: 'success', summary: 'Successful', detail: 'Product Created', life: 3000});
            }

          //  this.products = [...this.products];

          //  this.product = {};
        }

        */
    }

    findIndexById(id: string): number {
        let index = -1;
    //    for (let i = 0; i < this.products.length; i++) {
      //      if (this.products[i].id === id) {
      //          index = i;
       //         break;
        //    }
       // }

        return index;
    }

    createId(): string {
        let id = '';
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }
}
